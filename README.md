# SKA-SDP-HIP-FFT

This micro benchmark contains 1D and 2D FFT implementations using FFTW3, HIPFFT and CUFFT.

# Installation

Clone the repository and cd the folder:

```
git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-hip-fft.git
cd ska-sdp-hip-fft
```

Make sure to have CUDA/HIP (and HIPFFT) support on your system.
Please follow this [guidelines](https://git.astron.nl/RD/schaap-spack/-/wikis/Reproducible-SW-environment-with-Spack).
On NVIDIA systems the modules to load are: `cuda`, `cmake` and `hip`. 
Note: make sure to install HIPFFT with CUDA support ([spack recipe](https://git.astron.nl/RD/schaap-spack/)).
On AMD systems: `llvm-amdgpu`, `hip`, `hipfft`, `cmake`, `hip-rocclr`, `rocm-smi-lib`.

Source the setup file in the **share** folder:

```
source ./share/setup-env.sh
```

On an NVIDIA system also source the HIP-patch:
```
source ./share/setup-hip-nvidia.sh
```

```
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=../install
```

Check the benchmark you would like to build with the cmake GUI:

```
ccmake .
```

Then install:
```
make -j
make install
```

**NOTE**: there are also some scripts that can be used in the **scripts** folder.

To build the benchmark for CUDA you could simply run this in the main folder:
```
./scripts/install_nvidia.sh
```

### Run benchmark

Simply run the binaries created in `install` folder.
