# CMake find_package() Module for HIP  C++ runtime API
# https://github.com/ROCm-Developer-Tools/HIP
#
# Based on:
# https://github.com/ROCm-Developer-Tools/HIP/blob/main/cmake/FindHIP.cmake

if(NOT HIPFFT_ROOT_DIR)
    # Search in user specified path first
    if("${GPU_BRAND}" STREQUAL "NVIDIA")
        find_path(
            HIPFFT_ROOT_DIR
            NAMES include/hipfft.h
            PATHS
            "$ENV{HIPFFT_CUDA_ROOT}/hipfft"
            NO_DEFAULT_PATH
            )
    elseif("${GPU_BRAND}" STREQUAL "AMD")
        find_path(
            HIPFFT_ROOT_DIR
            NAMES include/hipfft.h
            PATHS
            "$ENV{ROCM_PATH}/hipfft"
            "$ENV{HIPFFT_ROOT}/hipfft"
            /opt/rocm/hipfft
            NO_DEFAULT_PATH
            )
    endif()

    if(NOT EXISTS ${HIPFFT_ROOT_DIR})
        if(HIPFFT_FIND_REQUIRED)
            message(FATAL_ERROR "Specify HIPFFT_ROOT_DIR")
        elseif(NOT HIPFFT_FIND_QUIETLY)
            message("HIPFFT_ROOT_DIR not found or specified")
        endif()
    endif()
    # And push it back to the cache
    set(HIPFFT_ROOT_DIR ${HIPFFT_ROOT_DIR} CACHE PATH "HIPFFT installed location" FORCE)
endif()

# Find HIP include directory
find_path(
    HIPFFT_INCLUDE_DIR
    NAMES hipfft.h
    HINTS ${HIPFFT_ROOT_DIR}
    PATH_SUFFIXES include
    NO_DEFAULT_PATH
)

# Find HIP library directory
find_library(
    HIPFFT_LIBRARY
    NAMES hipfft
    HINTS ${HIPFFT_ROOT_DIR}
    PATH_SUFFIXES lib
    NO_DEFAULT_PATH
)

message(${HIPFFT_ROOT_DIR})
message(${HIPFFT_INCLUDE_DIR})
message(${HIPFFT_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    HIPFFT
    REQUIRED_VARS
    HIPFFT_ROOT_DIR
    HIPFFT_INCLUDE_DIR
    HIPFFT_LIBRARY
    )
