#include "cpu-fftw3.hpp"

namespace cpu{
    void fft_1d(
        int size,
        fftwf_complex *input,
        fftwf_complex *output,
        int nr_iterations,
		int direction,
		int mode){
        fftwf_plan plan = fftwf_plan_dft_1d(size, input, output, direction, mode);
        
        common::timer t;
        
        for(int i = 0; i < nr_iterations; i++){
            t.start();
            fftwf_execute(plan);
            t.stop();
            std::cout << "Time Elapsed: " << t.time_elapsed() << std::endl;
        }

        fftwf_destroy_plan(plan);
        fftwf_cleanup();
    }
    void fft_1d(
        int size,
        std::vector<fftwf_complex> &input,
        std::vector<fftwf_complex> &output,
        int nr_iterations,
		int direction,
		int mode){
        fft_1d(size, input.data(), output.data(), direction, mode, nr_iterations);
    }

    void fft_2d(
		int size1,
		int size2,
		fftwf_complex *input,
		fftwf_complex *output,
        int nr_iterations,
		int direction,
		int mode){
		
		fftwf_plan plan = fftwf_plan_dft_2d(size1, size2, input, output, direction, mode);
		
		common::timer t;

        for(int i = 0; i < nr_iterations; i++){
            t.start();
            fftwf_execute(plan);
            t.stop();
            std::cout << "Time elapsed: " << t.time_elapsed() << std::endl;
        }

		fftwf_destroy_plan(plan);
		fftwf_cleanup();
	}

	void fft_2d(
		int size1,
		int size2,
		std::vector<fftwf_complex> &input,
		std::vector<fftwf_complex> &output,
        int nr_iterations,
		int direction,
		int mode){
		fft_2d(size1, size2, input.data(), output.data(), direction, mode, nr_iterations);
		
	}

    void initialize_fftwf_complex(fftwf_complex *vec, int size){
        #pragma omp parallel for
        for(int i = 0; i < size; i++){
            vec[i][0] = common::gen_rand();
            vec[i][1] = common::gen_rand();
        }
    }

    void initialize_fftwf_complex(std::vector<fftwf_complex> &vec){
        initialize_fftwf_complex(vec.data(), vec.size());
    }
}