#pragma once

#include "common.hpp"
#include <fftw3.h>
#include <omp.h>

namespace cpu{
    void fft_1d(
        int size,
        fftwf_complex *input,
        fftwf_complex *output,
        int nr_iterations = 1,
        int direction = FFTW_FORWARD,
        int mode = FFTW_ESTIMATE);

    void fft_1d(
        int size,
        std::vector<fftwf_complex> &input,
        std::vector<fftwf_complex> &output,
        int direction = FFTW_FORWARD,
        int nr_iterations = 1,
        int mode = FFTW_ESTIMATE);

    void fft_2d(
		int size1,
		int size2,
		fftwf_complex *input,
		fftwf_complex *output,
        int nr_iterations = 1,
		int direction = FFTW_FORWARD,
		int mode = FFTW_ESTIMATE);

	void fft_2d(
		int size1,
		int size2,
		std::vector<fftwf_complex> &input,
		std::vector<fftwf_complex> &output,
        int nr_iterations = 1,
		int direction = FFTW_FORWARD,
		int mode = FFTW_ESTIMATE);

    void initialize_fftwf_complex(fftwf_complex *vec, int size);

    void initialize_fftwf_complex(std::vector<fftwf_complex> &vec);
}