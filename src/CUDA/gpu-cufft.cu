#include "gpu-cufft.cuh"

namespace cuda{
    void header(){
        std::cout << "HELLO" << std::endl;
    }
    	void fft_1d_C2C(
		int size,
		std::vector<cufftComplex> &input,
		std::vector<cufftComplex> &output,
		int nr_iterations,
		int direction)
	{
		cufftHandle plan;
		cufftComplex *data_in_d;
		cufftComplex *data_out_d;
		
		common::timer t;
		
		cudaMalloc((void**)&data_in_d, sizeof(cufftComplex) * size);
		cudaMalloc((void**)&data_out_d, sizeof(cufftComplex) * size);
		
		cudaMemcpy(data_in_d, input.data(), sizeof(cufftComplex) * size, cudaMemcpyHostToDevice);
		
		cufftPlan1d(&plan, size, CUFFT_C2C, 1); //BATCH = 1
		
		for(int i = 0; i < nr_iterations; i++){
			t.start();
			cufftExecC2C(plan, data_in_d, data_out_d, direction);
			cudaDeviceSynchronize();
			t.stop();
			std::cout << "Time elapsed: " << t.time_elapsed() << std::endl;
		}
		
		
		cudaMemcpy(output.data(), data_out_d, sizeof(cufftComplex) * size, cudaMemcpyDeviceToHost);
		
		cufftDestroy(plan);
		cudaFree(data_in_d);
		cudaFree(data_out_d);
	}

    void fft_2d_C2C(
		int size1,
		int size2,
		std::vector<cufftComplex> &input,
		std::vector<cufftComplex> &output,
		int nr_iterations,
		int direction)
	{
		cufftHandle plan;
		cufftComplex *data_in_d;
		cufftComplex *data_out_d;
		
		common::timer t;
		
		cudaMalloc((void**)&data_in_d, sizeof(cufftComplex) * size1 * size2);
		cudaMalloc((void**)&data_out_d, sizeof(cufftComplex) * size1 * size2);
		
		cudaMemcpy(data_in_d, input.data(), sizeof(cufftComplex) * size1 * size2, cudaMemcpyHostToDevice);
		
		cufftPlan2d(&plan, size1, size2, CUFFT_C2C); 
		
		for(int i = 0; i < nr_iterations; i++){
			t.start();
			cufftExecC2C(plan, data_in_d, data_out_d, direction);
			cudaDeviceSynchronize();
			t.stop();
			std::cout << "Time elapsed: " << t.time_elapsed() << std::endl;
		}

		cudaMemcpy(output.data(), data_out_d, sizeof(cufftComplex) * size1 * size2, cudaMemcpyDeviceToHost);
		
		cufftDestroy(plan);
		cudaFree(data_in_d);
		cudaFree(data_out_d);
	}


    void initialize_cufft_complex(std::vector<cufftComplex> &vec){
        #pragma omp parallel for
        for(int i = 0; i < vec.size(); i++){
            vec[i].x = common::gen_rand();
            vec[i].y = common::gen_rand();
        }
    }

	void translateFFTWFtoCUFFTvector( std::vector<cufftComplex> &gpu, std::vector<fftwf_complex> &cpu){
		for(int i = 0; i < cpu.size(); i++)
		{
			gpu[i] = make_float2(cpu[i][0], cpu[i][1]);
		}
	}


	void compare_cufft_fftw(std::vector<cufftComplex> &cufft_vec, std::vector<fftwf_complex> &fftw_vec){
		float max = cufft_vec[0].x-fftw_vec[0][0];
		for(int i = 0; i < cufft_vec.size(); i++){
			if(abs(cufft_vec[i].x-fftw_vec[i][0]) > abs(cufft_vec[i].y-fftw_vec[i][1])){
				max = abs(cufft_vec[i].x-fftw_vec[i][0]);
			}
			else{
				max = abs(cufft_vec[i].y-fftw_vec[i][1]);
			}		
		}
		std::cout << "Max error: " << max << std::endl;
	}
}