#pragma once

#include <cufft.h>
#include <omp.h>
#include "common.hpp"
#include <fftw3.h>

namespace cuda{
    void fft_1d_C2C(
		int size,
		std::vector<cufftComplex> &input,
		std::vector<cufftComplex> &output,
		int nr_iterations = 1,
		int direction = CUFFT_FORWARD);

	void fft_2d_C2C(
		int size1,
		int size2,
		std::vector<cufftComplex> &input,
		std::vector<cufftComplex> &output,
		int nr_iterations = 1,
		int direction = CUFFT_FORWARD);

	void initialize_cufft_complex(std::vector<cufftComplex> &vec);

	void translateFFTWFtoCUFFTvector(std::vector<cufftComplex> &gpu, std::vector<fftwf_complex> &cpu);

	void compare_cufft_fftw(std::vector<cufftComplex> &cufft_vec, std::vector<fftwf_complex> &fftw_vec);
}