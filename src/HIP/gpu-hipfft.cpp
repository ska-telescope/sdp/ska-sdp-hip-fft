#include "gpu-hipfft.hpp"

namespace hip{
    	void fft_1d_C2C(
		int size,
		std::vector<hipfftComplex> &input,
		std::vector<hipfftComplex> &output,
		int nr_iterations,
		int direction)
	{
		hipfftHandle plan;
		hipfftComplex *data_in_d;
		hipfftComplex *data_out_d;
		
		common::timer t;
		
		hipMalloc((void**)&data_in_d, sizeof(hipfftComplex) * size);
		hipMalloc((void**)&data_out_d, sizeof(hipfftComplex) * size);
		
		hipMemcpy(data_in_d, input.data(), sizeof(hipfftComplex) * size, hipMemcpyHostToDevice);
		
		hipfftPlan1d(&plan, size, HIPFFT_C2C, 1); //BATCH = 1
		
		for(int i = 0; i < nr_iterations; i++){
			t.start();
			hipfftExecC2C(plan, data_in_d, data_out_d, direction);
			hipDeviceSynchronize();
			t.stop();
			std::cout << "Time elapsed: " << t.time_elapsed() << std::endl;
		}

		hipMemcpy(output.data(), data_out_d, sizeof(hipfftComplex) * size, hipMemcpyDeviceToHost);
		
		hipfftDestroy(plan);
		hipFree(data_in_d);
		hipFree(data_out_d);
	}

    void fft_2d_C2C(
		int size1,
		int size2,
		std::vector<hipfftComplex> &input,
		std::vector<hipfftComplex> &output,
		int nr_iterations,
		int direction)
	{
		hipfftHandle plan;
		hipfftComplex *data_in_d;
		hipfftComplex *data_out_d;
		
		common::timer t;
		
		hipMalloc((void**)&data_in_d, sizeof(hipfftComplex) * size1 * size2);
		hipMalloc((void**)&data_out_d, sizeof(hipfftComplex) * size1 * size2);
		
		hipMemcpy(data_in_d, input.data(), sizeof(hipfftComplex) * size1 * size2, hipMemcpyHostToDevice);
		
		hipfftPlan2d(&plan, size1, size2, HIPFFT_C2C); 
		
		for(int i = 0; i < nr_iterations; i++){
			t.start();
			hipfftExecC2C(plan, data_in_d, data_out_d, direction);
			hipDeviceSynchronize();
			t.stop();
			std::cout << "Time elapsed: " << t.time_elapsed() << std::endl;
		}
		
		hipMemcpy(output.data(), data_out_d, sizeof(hipfftComplex) * size1 * size2, hipMemcpyDeviceToHost);
		
		hipfftDestroy(plan);
		hipFree(data_in_d);
		hipFree(data_out_d);
	}


    void initialize_hipfft_complex(std::vector<hipfftComplex> &vec){
        #pragma omp parallel for
        for(int i = 0; i < vec.size(); i++){
            vec[i].x = common::gen_rand();
            vec[i].y = common::gen_rand();
        }
    }

	void translateFFTWFtoHIPFFTvector(std::vector<hipfftComplex> &gpu, fftwf_complex *cpu){
		for(int i = 0; i < gpu.size(); i++){
			gpu[i].x = cpu[i][0];
			gpu[i].y = cpu[i][1];
		}
	}

	void compare_hipfft_fftw(std::vector<hipfftComplex> &hipfft_vec, fftwf_complex *fftw_vec){
		float max = hipfft_vec[0].x-fftw_vec[0][0];
		for(int i = 0; i < hipfft_vec.size(); i++){
			if(abs(hipfft_vec[i].x-fftw_vec[i][0]) > abs(hipfft_vec[i].y-fftw_vec[i][1])){
				max = abs(hipfft_vec[i].x-fftw_vec[i][0]);
			}
			else{
				max = abs(hipfft_vec[i].y-fftw_vec[i][1]);
			}		
		}
		std::cout << "Max error: " << max << std::endl;
	}
}