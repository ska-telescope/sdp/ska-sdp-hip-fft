#pragma once

#include <hipfft.h>
#include <omp.h>
#include "common.hpp"
#include <fftw3.h>

namespace hip{
    void fft_1d_C2C(
                int size,
                std::vector<hipfftComplex> &input,
                std::vector<hipfftComplex> &output,
                int nr_iterations = 1,
                int direction = HIPFFT_FORWARD);

        void fft_2d_C2C(
                int size1,
                int size2,
                std::vector<hipfftComplex> &input,
                std::vector<hipfftComplex> &output,
                int nr_iterations = 1,
                int direction = HIPFFT_FORWARD);

        void initialize_hipfft_complex(std::vector<hipfftComplex> &vec);

        void translateFFTWFtoHIPFFTvector(std::vector<hipfftComplex> &gpu, fftwf_complex *cpu);

	void compare_hipfft_fftw(std::vector<hipfftComplex> &hipfft_vec, fftwf_complex *fftw_vec);
}