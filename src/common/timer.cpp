#include "timer.hpp"

namespace common{              
    void timer::start(){
        t_start = high_resolution_clock::now();
    }
    
    void timer::stop(){
        t_end = high_resolution_clock::now();
    }
    
    double timer::time_elapsed(){
        duration<double, std::milli> ms_double = t_end - t_start;
        return ms_double.count() * 1e-3;
    }
    
    void timer::reset(){
        t_start = t_end = {};
    }
}