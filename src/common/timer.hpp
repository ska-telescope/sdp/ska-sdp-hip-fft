#pragma once

#include <chrono>
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
using std::chrono::system_clock;

namespace common{
    class timer{
        private:
            system_clock::time_point t_start;
            system_clock::time_point t_end;
        public:            
            void start();        
            void stop();        
            double time_elapsed();        
            void reset();
    };
}