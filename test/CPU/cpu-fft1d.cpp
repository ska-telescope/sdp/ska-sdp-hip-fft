#include "common.hpp"
#include "cpu-fftw3.hpp"

int main(int argc, char **argv){
    std::cout << ">>> CPU FFTW3" << std::endl;
    if(argc != 3){
		std::cout << "Please give the FFT SIZE and the NUMBER of ITERATIONS as input parameters" << std::endl;
		return 0;	
	}
	
	int SIZE = atoi(argv[1]);
    int ITERATIONS = atoi(argv[2]);

    std::vector<fftwf_complex> a(SIZE);
	std::vector<fftwf_complex> b(SIZE);

    cpu::initialize_fftwf_complex(a);

    cpu::fft_1d(SIZE, a, b, ITERATIONS);
}