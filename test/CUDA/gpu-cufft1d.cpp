#include "common.hpp"
#include "gpu-cufft.cuh"

int main(int argc, char **argv){
    std::cout << ">>> GPU CUFFT" << std::endl;
    if(argc != 3){
		std::cout << "Please give the FFT SIZE and the NUMBER of ITERATIONS as input parameters" << std::endl;
		return 0;	
	}
	
	int SIZE = atoi(argv[1]);
    int ITERATIONS = atoi(argv[2]);

    std::vector<cufftComplex> a(SIZE);
	std::vector<cufftComplex> b(SIZE);

    cuda::initialize_cufft_complex(a);

    cuda::fft_1d_C2C(SIZE, a, b, ITERATIONS);
}