#include "common.hpp"
#include "gpu-cufft.cuh"
#include "cpu-fftw3.hpp"

int main(int argc, char **argv){
    std::cout << ">>> GPU CUFFT" << std::endl;
    if(argc != 2){
		std::cout << "Please give the FFT SIZE as input parameter" << std::endl;
		return 0;	
	}
	
	int SIZE = atoi(argv[1]);

    std::vector<cufftComplex> a_gpu(SIZE);
	std::vector<cufftComplex> b_gpu(SIZE);

    std::vector<fftwf_complex> a_cpu(SIZE);
	std::vector<fftwf_complex> b_cpu(SIZE);


    cpu::initialize_fftwf_complex(a_cpu);
    cuda::translateFFTWFtoCUFFTvector(a_gpu, a_cpu);

    cuda::fft_1d_C2C(SIZE, a_gpu, b_gpu);
    cpu::fft_1d(SIZE, a_cpu, b_cpu);

    cuda::compare_cufft_fftw(b_gpu, b_cpu);
}



