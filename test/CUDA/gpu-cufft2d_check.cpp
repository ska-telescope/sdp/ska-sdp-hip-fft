#include "common.hpp"
#include "gpu-cufft.cuh"
#include "cpu-fftw3.hpp"

int main(int argc, char **argv){
    std::cout << ">>> GPU CUFFT" << std::endl;
    if(argc != 2){
		std::cout << "Please give the FFT SIZE as input parameter" << std::endl;
		return 0;	
	}
	
	int SIZE = atoi(argv[1]);

    std::vector<cufftComplex> a_gpu(SIZE * SIZE);
	std::vector<cufftComplex> b_gpu(SIZE * SIZE);

    std::vector<fftwf_complex> a_cpu(SIZE * SIZE);
	std::vector<fftwf_complex> b_cpu(SIZE * SIZE);


    cpu::initialize_fftwf_complex(a_cpu);
    cuda::translateFFTWFtoCUFFTvector(a_gpu, a_cpu);

    cuda::fft_2d_C2C(SIZE, SIZE, a_gpu, b_gpu);
    cpu::fft_2d(SIZE, SIZE, a_cpu, b_cpu);

    cuda::compare_cufft_fftw(b_gpu, b_cpu);
}



