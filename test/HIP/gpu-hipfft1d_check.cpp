#include "common.hpp"
#include "gpu-hipfft.hpp"
#include "cpu-fftw3.hpp"

int main(int argc, char **argv){
    std::cout << ">>> GPU HIPFFT" << std::endl;
    if(argc != 2){
		std::cout << "Please give the FFT SIZE as input parameter" << std::endl;
		return 0;	
	}
	
	int SIZE = atoi(argv[1]);

    std::vector<hipfftComplex> a_gpu(SIZE);
    std::vector<hipfftComplex> b_gpu(SIZE);

    fftwf_complex* a_cpu;
    fftwf_complex* b_cpu;
    a_cpu = (fftwf_complex*) malloc(SIZE * sizeof(fftwf_complex));
    b_cpu = (fftwf_complex*) malloc(SIZE * sizeof(fftwf_complex));
    cpu::initialize_fftwf_complex(a_cpu, SIZE);
    
    hip::translateFFTWFtoHIPFFTvector(a_gpu, a_cpu);


    hip::fft_1d_C2C(SIZE, a_gpu, b_gpu);
    cpu::fft_1d(SIZE, a_cpu, b_cpu);

    hip::compare_hipfft_fftw(b_gpu, b_cpu);
}