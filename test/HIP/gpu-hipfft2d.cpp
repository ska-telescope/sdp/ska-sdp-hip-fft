#include "common.hpp"
#include "gpu-hipfft.hpp"

int main(int argc, char **argv){
    std::cout << ">>> GPU HIPFFT" << std::endl;
    if(argc != 3){
		std::cout << "Please give the FFT SIZE and the NUMBER of ITERATIONS as input parameters" << std::endl;
		return 0;	
	}
	
	int SIZE = atoi(argv[1]);
    int ITERATIONS = atoi(argv[2]);

    std::vector<hipfftComplex> a(SIZE * SIZE);
	std::vector<hipfftComplex> b(SIZE * SIZE);

    hip::initialize_hipfft_complex(a);

    hip::fft_2d_C2C(SIZE, SIZE, a, b, ITERATIONS);
}